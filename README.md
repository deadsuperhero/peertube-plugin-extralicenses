# PeerTube plugin Extra Licenses

This plugin is based on the excellent [PeerTube Creative Commons](https://github.com/beeldengeluid/peertube-plugin-creative-commons) plugin, but exists to add non-cc licenses that
were not originally covered.

### Licenses list

1. All Rights Reserved
2. Royalty-Free License
3. Open Publication License
4. Free Art License 1.3

### Licenses badges

For every chosen license a corresponding badge is shown on the video watch page which links to the license deed.

These badges are custom and reflect the styling of badges found on https://licensebuttons.net/

### How to use

* Login as admin in your PeerTube instance
* Go to Plugin/Themes in the Administration section
* Search plugins for 'license'
* Click on Install
