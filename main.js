async function register ({
  registerHook,
  videoLicenceManager,
}) {
  videoLicenceManager.addConstant(9, 'All Rights Reserved')
  videoLicenceManager.addConstant(10, 'Royalty-Free License')
  videoLicenceManager.addConstant(11, 'Open Publication License')
  videoLicenceManager.addConstant(12, 'Free Art License 1.3')
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
