function register ({ registerHook, peertubeHelpers }) {

  const baseStaticUrl = peertubeHelpers.getBaseStaticRoute()

  const EXTRA_VIDEO_LICENCES = {
    9: {
      label: "All Rights Reserved",
      image: baseStaticUrl + "/images/cr.png",
      href: "https://en.wikipedia.org/wiki/Copyright"
    },
    10: {
      label: "Royalty-Free License",
      image: baseStaticUrl + "/images/rf.png",
      href: "https://en.wikipedia.org/wiki/Royalty-free"
    },
    11: {
      label: "Open Publication License",
      image: baseStaticUrl + "/images/opl.png",
      href: "https://opencontent.org/openpub/"
    },
    12: {
      label: "Free Art License 1.3",
      image: baseStaticUrl + "/images/cl.png",
      href: "https://artlibre.org/licence/lal/en/"
    }
  }

  registerHook({
    target: 'filter:api.video-watch.video.get.result',
    handler: video => {
      if (video.licence.id >= 9 && video.licence.id <= 12) {
        video.licence.image = EXTRA_VIDEO_LICENCES[video.licence.id].image
        video.licence.href = EXTRA_VIDEO_LICENCES[video.licence.id].href
      }
      return video
    }
  })

  registerHook({
    target: 'action:video-watch.player.loaded',
    handler: ({ videojs, video, playlist }) => {
      {
        // Match all nodes

        // match multiple elements, defined to handle responsiveness
        // see https://github.com/Chocobozzz/PeerTube/blob/33eb19e5199cc9fa4d73c6675c97508e3e072ef9/client/src/app/%2Bvideos/%2Bvideo-watch/video-watch.component.html#L55-L56

        const video_info = document.querySelectorAll('.video-info')
        const video_info_name = document.querySelectorAll('.video-info-name')
        const video_info_date_views = document.querySelectorAll('.video-info-date-views')
        const extra_licence = document.querySelectorAll('.extra-licence')
        const account_page_link = document.querySelector('[title="Account page"]');

        // Remove everything before setting for newly selected video

        for (let element of video_info) {
          element.removeAttribute('xmlns:dct')
          element.removeAttribute('xmlns:cc')
        }

        for (let element of video_info_name) {
          element.removeAttribute('property')
        }

        for (let element of extra_licence) {
          element.remove()
        }

        if (account_page_link) {
          account_page_link.firstElementChild.removeAttribute('property')
          account_page_link.removeAttribute('rel')
        }

        if (video.licence.id >= 9 && video.licence.id <= 12) {

          // Insert licence buttonlink

          const licence_span = document.createElement('span')
          licence_span.className = 'extra-licence'
          licence_span.innerHTML = ' • '

          const licence_link = document.createElement('a')
          licence_link.rel = 'license'
          licence_link.href = video.licence.href
          licence_link.target = '_blank'

          const licence_button = document.createElement('img')
          licence_button.src = video.licence.image

          licence_link.appendChild(licence_button)
          licence_span.appendChild(licence_link)

          for (let element of video_info_date_views) {
            element.insertAdjacentHTML('beforeend', licence_span.outerHTML)
          }

        }
      }
    }
  })
}

export {
  register
}
